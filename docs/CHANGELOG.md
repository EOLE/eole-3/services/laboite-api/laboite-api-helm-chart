# Changelog

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/compare/release/1.1.0...release/1.2.0) (2024-06-12)


### Features

* update appVersion to 1.0.2 ([f856c82](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/f856c820458f0a38fd2649811b8fdf2d0d9d8b48))


### Bug Fixes

* restore testing tag ([0fd55cf](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/0fd55cf045dd276772fa06bbdbf218c433dfd208))
* wait 5 minutes before killing pod ([c3c4cbb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/c3c4cbbd04df7a3973d0b11f3ec538033e53fbb1))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/compare/release/1.0.0...release/1.1.0) (2024-01-16)


### Features

* update app version to 1.0.1 ([24c0ec5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/24c0ec5657f625c9fbe9aa79b8bebc600929e370))

## 1.0.0 (2023-10-17)


### Features

* add laboite-api helm and ci files ([08dddcb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/08dddcbaefca7b08b99262f0291b24474cd53c48))
* first stable version ([b609b2d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/b609b2db21168518ae89b09a135f47b08a2df21b))
* first testing version ([37b9d90](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/37b9d90596c7a3d349d46b32fb680609349a720e))


### Bug Fixes

* add README file for laboite-api ([002b3eb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/002b3eba87f2a045f5777fe230d040d20d07abb8))
* correct default values and the name of laboite-api to laboiteapi ([7be0fff](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/7be0fff5be77cfb1b63cf8d962f976393e125b33))
* delete the README file and tack off the tag in the values.yaml ([495edcf](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite-api/laboite-api-helm-chart/commit/495edcf1dd192213e0f3d204f9ad371a001d6f6c))
